/*
 * Exam201801Q1.cpp
 *
 *  Created on: May 2, 2019
 *      Author: ruby
 */
#include <iostream>

using namespace std;
class Exam201801Q1 {
public:
	/* O(n^2 )*/
	int countNghichThe1(int a[], int n) {
		int count = 0;
		for (int i = 0; i < n - 1; i++) {
			for (int j = i + 1; j < n; j++) {
				if (a[i] > a[j]) {
					count++;
					cout << "(" << a[i] << "," << a[j] << ")" << endl;
				}
			}
		}
		return count;
	}

	/* O(nlogn) */
	int countNghichThe2(int a[], int n) {
		// Độ phức tạp O(nlogn)
		int count = 0;
		// TODO
		return count;
	}
};

// a) 5 nghịch thế (2,4), (2,5), (3,4), (3,5), (4,5)
// Lưu ý nghịch thế là cặp (i,j), không phải giá trị, index tính từ 1-n

// b) Số nghịch thế nhiều nhất khi mảng được sắp xếp theo thứ tự giảm dần --> Số nghịch thế là n*(n-1)/2

//int main(int argc, char **argv) {
//	Exam201801Q1 test;
//	int n = 5;
//	int a[n] = { 1, 4, 5, 3, 2 };
//	cout << "So nghich the = " << test.countNghichThe1(a, n) << endl;
//}

