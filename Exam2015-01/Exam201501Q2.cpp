/*
 * Exam201501Q2.cpp
 *
 *  Created on: May 4, 2019
 *      Author: ruby
 */
#include <iostream>
using namespace std;
struct BinSearchTree {
	unsigned int key;
	BinSearchTree *left, *right;

	unsigned int findMaxKey(BinSearchTree *btree) {
		BinSearchTree* _tree = btree;
		while (_tree->right != NULL) {
			_tree = _tree->right;
		}
		return _tree->key;
	}

	BinSearchTree *findMaxEvenLessThan(BinSearchTree *btree, unsigned int x) {
		if (btree == NULL)
			return NULL;

		if (btree->key > x)
			return findMaxEvenLessThan(btree->left, x);

		BinSearchTree *kq = findMaxEvenLessThan(btree->left, x);

		if (kq != NULL)
			return kq;

		if (btree->key % 2 == 0)
			return btree;

		return findMaxEvenLessThan(btree->left, x);
	}

	BinSearchTree* search(BinSearchTree* root, unsigned int x) {
		if (root == NULL) {
			return NULL;
		}
		if (root->key == x) {
			return root;
		}
		if (x < root->key) {
			return search(root->left, x);
		}
		if (x > root->key) {
			return search(root->right, x);
		}
	}

	BinSearchTree * repeatSearch(BinSearchTree* root, BinSearchTree *parent,
			unsigned int x) {
		if (root == NULL) {
			return NULL;
		}
		if (root->key == x) {
			return root;
		}

		if (x < root->key) {
			parent = root;
			return repeatSearch(root->left, parent, x);
		}
		if (x > root->key) {
			parent = root;
			return repeatSearch(root->right, root, x);
		}
	}

	void insert(BinSearchTree *root, unsigned int x) {
		if (root == NULL) {
			root->key = x;
			root->left = NULL;
			root->right = NULL;
		} else if (x < root->key) {
			insert(root->left, x);
		} else if (x > root->key) {
			insert(root->right, x);
		}
	}

	bool IsLeaf(BinSearchTree * bTree) {
		return bTree->left == NULL && bTree->right == NULL;
	}

	int countTotalNode(BinSearchTree * bTree) {
		if (bTree == NULL) {
			return 0;
		}
		return 1 + countTotalNode(bTree->left) + countTotalNode(bTree->right);
	}

	BinSearchTree* findMinNode(BinSearchTree *root) {
		if (root == NULL) {
			return NULL;
		} else {
			return findMinNode(root->left);
		}
	}

	void deleteMin(BinSearchTree *root) {
		if (root == NULL) {
			return;
		}
		if (root->left == NULL) {
			root = root->right;
		} else {
			deleteMin(root->left);
		}
	}

	BinSearchTree * findMaxNode(BinSearchTree *root) {
		if (root == NULL) {
			return NULL;
		} else {
			return findMaxNode(root->right);
		}
	}

	void deleteMax(BinSearchTree *root) {
		if (root == NULL) {
			return;
		}
		if (root->right == NULL) {
			root = root->left;
		} else {
			deleteMax(root->right);
		}
	}

	/**
	 * Remove a node with key
	 */
	void remove(BinSearchTree *root, unsigned int x) {
		BinSearchTree * parent = NULL;
		BinSearchTree *node = repeatSearch(root, parent, x);
		if (node == NULL) {
			// empty tree
			return;
		}
		if (node->right != NULL) {
			BinSearchTree * tmp = findMinNode(node->right);
			node->key = tmp->key;
			tmp = NULL;
		} else if (node->left != NULL) {
			BinSearchTree *tmp = findMaxNode(node->left);
			node->key = tmp->key;
			key = NULL;
		} else {
			// leaf node
			if (parent == NULL) {
				root = NULL;
				return;
			}
			if (x < parent->key) {
				parent->left = NULL;
			} else {
				parent->right = NULL;
			}
		}

	}
};

