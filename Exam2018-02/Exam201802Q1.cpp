/*
 * Exam201802Q1.cpp
 *
 *  Created on: May 2, 2019
 *      Author: ruby
 */

#include <iostream>

using namespace std;

#define MAX 1000
#define Inf 1000000000

/******* a *******
 * 	2	4	4	5
 * 	6	7	10	19
 * 	20	21	∞	∞
 * 	∞	∞	∞	∞
 */

class Exam201802Q1 {

public:
	//
	bool IsYoungTableau(int Y[][MAX], int m, int n) {
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (i < m - 1 && Y[i][j] > Y[i + 1][j])
					return false;
				if (j < n - 1 && Y[i][j] > Y[i][j + 1])
					return false;
			}
		}
		return true;
	}

	void ConvertToYoung(int A[], int k, int Y[][MAX], int m, int n) {
		for (int i = 0; i < k; i++) {
			if (Y[m - 1][n - 1] != Inf)
				return;
			Y[m - 1][n - 1] = A[i];
			correctInsert(Y, m, n, m - 1, n - 1);
		}

	}

	void correctInsert(int Y[][MAX], int m, int n, int x, int y) {
		int max = Y[x][y], i, j;
		if (x > 0 && Y[x - 1][y] > max) {
			max = Y[x - 1][y];
			i = x - 1;
			j = y;
		}
		if (y > 0 && Y[x][y - 1] > max) {
			max = Y[x][y - 1];
			i = x;
			j = y - 1;
		}
		if (Y[x][y] < max) {
			Y[i][j] = Y[x][y];
			Y[x][y] = max;
			correctInsert(Y, m, n, i, j);
		}

	}

	void RemoveElement(int Y[][MAX], int m, int n, int k, int h) {
		Y[k][h] = Inf;
		correctRemove(Y, m, n, k, h);
	}

	void correctRemove(int Y[][MAX], int m, int n, int x, int y) {
		int min = Y[x][y], i, j;
		if (x < m - 1 && Y[x + 1][y] < min) {
			min = Y[x + 1][y];
			i = x + 1;
			j = y;
		}
		if (y < n - 1 && Y[x][y + 1] < min) {
			min = Y[x][y + 1];
			i = x;
			j = y + 1;
		}
		if (Y[x][y] > min) {
			Y[i][j] = Y[x][y];
			Y[x][y] = min;
			correctRemove(Y, m, n, i, j);
		}
	}

	void print(int Y[][MAX], int m, int n) {
		cout << "************************" << endl;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				cout << Y[i][j] << "\t";
			}
			cout << endl;
		}
	}

};

//int main(int argc, char **argv) {
//	Exam201802Q1 test;
//	int Y[][MAX] = { { 1, 2 }, { 1, 3 }, { 1, 4 } };
//	int m = 3, n = 2;
//	cout << "Is Young Tableau? " << test.IsYoungTableau(Y, m, n) << endl;
//	test.print(Y, m, n);
//
//	test.RemoveElement(Y, m, n, 0, 0);
//	test.print(Y, m, n);
//
//	int A[] = { 5, 9	, 4, 3, 9, 11, 35, 0 };
//	int k = 8;
//	int M[MAX][MAX] = { { Inf, Inf }, { Inf, Inf }, { Inf, Inf } };
//	test.ConvertToYoung(A, k, M, m, n);
//	test.print(M, m, n);
//}

