/*
 * ExamblQ2.cpp
 *
 *  Created on: May 1, 2019
 *      Author: ruby
 */

#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

struct Phim {
	string Ten;
	string NuocSanXuat;
	int NamSanXuat;
	string TheLoai;
	float DiemDanhGia;
	Phim * pNext;

	Phim * ThemPhim(Phim*& pHead, string Ten, string NuocSanXuat,
			int NamSanXuat, string TheLoai, float DiemDanhGia) {

		Phim * p = new Phim;
		p->Ten = Ten;
		p->NuocSanXuat = NuocSanXuat;
		p->NamSanXuat = NamSanXuat;
		p->TheLoai = TheLoai;
		p->DiemDanhGia = DiemDanhGia;
		p->pNext = pHead;

		return p;

	}

	int SoLuongPhimTheoNamSanXuat(Phim* pHead, int NamSanXuat) {
		int count = 0;
		Phim * p = pHead;
		while (p != NULL) {
			if (p->NamSanXuat == NamSanXuat) {
				count++;
				p = p->pNext;
			}
		}
		return count;
	}

	Phim * TimKiemPhim(Phim *pHead, string strTheLoai, float minDiemDanhGia) {
		Phim * p = pHead;
		Phim * kq = NULL;
		while (p != NULL) {
			if (p->TheLoai == strTheLoai && p->DiemDanhGia >= minDiemDanhGia) {
				Phim * newPhim = new Phim;
				memcpy(newPhim, p, sizeof(p));
				newPhim->pNext = kq;
			}
			p = p->pNext;
		}
		return kq;
	}
};

