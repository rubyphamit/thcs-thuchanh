/*
 * Exam201501Q3.cpp
 *
 *  Created on: May 1, 2019
 *      Author: ruby
 */

#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <iostream>
using namespace std;

class Exam201501Q4 {
public:
	bool checkTangGiam(int a[], int n) {
		if (n < 3) {
			return false;
		}
		bool result = false;
		int k;
		for (int i = 1; i < n - 1; i++) {
			if (a[i] > a[i + 1]) {
				k = i;
				result = true;
			}
		}
		for (int i = k + 1; i < n; i++) {
			if (a[i] < a[i + 1]) {
				result = false;
			}
		}
		return result;
	}

	int find_k(int a[], int n) {
		int left = 1, right = n - 2, k = (left + right) / 2;
		int kq;
		while (left <= right) {
			kq = a[k];
			if (a[k] > a[k + 1]) {
				right = k - 1;
			} else if (a[k] < a[k + 1]) {
				left = k + 1;
			}
			k = (left + right) / 2;
		}
		return kq;
	}
};

//int main(int argc, char **argv) {
//	Exam201501Q4 test;
//	int n = 8;
//	int a[n] = { 0, 1, 2, 3, 4, 5, 7, 9 };
//	printf("Check tang giam = %s\n",
//			test.checkTangGiam(a, n) ? "true" : "false");
//	int b[n] = { 0, 100, 20, 13, 9, 5, 4, 0 };
//	printf("Check tang giam = %s\n",
//			test.checkTangGiam(b, n) ? "true" : "false");
//
//	printf("max = %d\n", test.find_k(b, n));
//	int c[n] = { 0, 1, 2, 13, 90, 500, 4000, 0 };
//	printf("max = %d\n", test.find_k(c, n));
//}

