#include <iostream>

using namespace std;
class ComplexNum {
private:
	float m_Real;
	float m_Img;
public:
	ComplexNum() {
		m_Real = 0;
		m_Img = 0;
	}

	ComplexNum(float real, float img = 0) {
		m_Real = real;
		m_Img = img;
	}

	virtual ~ComplexNum() {
		cout << m_Real << "," << m_Img << endl;
	}

};

//int main(int argc, char **argv) {
//	ComplexNum x(5, 3);
//	ComplexNum y(7);
//	ComplexNum *p = new ComplexNum(9, 8);
////	z= &x;
////	delete z;
//
////	delete (p);
////	p = &x;
//	return 0;
//}
