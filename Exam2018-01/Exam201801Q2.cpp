/*
 * Exam201801Q2.cpp
 *
 *  Created on: May 2, 2019
 *      Author: ruby
 */

class Exam201801Q2 {

	bool CheckStatus(int nX, int X[]) {
		for (int i = 0; i < nX - 1; i++) {
			if (X[i] < X[i + 1]) {
				return false;
			}
		}
		return true;
	}

	bool IsValidMove(int nX, int X[], int nY, int Y[]) {
		if (nX == 0) {
			return false;
		}
		if (nY == 0) {
			return true;
		}
		if (X[nX - 1] < Y[nY - 1]) {
			return true;
		}
		return false;
	}

	int CountValidMoves(int nA, int A[], int nB, int B[], int nC, int C[]) {
		int count = 0;
		if (IsValidMove(nA, A, nB, B) || IsValidMove(nB, B, nA, A)) {
			count++;
		}
		if (IsValidMove(nA, A, nC, C) || IsValidMove(nC, C, nA, A)) {
			count++;
		}
		if (IsValidMove(nB, B, nC, C) || IsValidMove(nC, C, nB, B)) {
			count++;
		}
		return count;
	}

	// g)
	/**
	 *	an = 2 * an_1 + 1;
	 *	a1 = 1;
	 *	a2 = 3;
	 *	a3 = 7;
	 *
	 * Số lần chuyển n-1 đĩa trên cùng từ cọc A sang cọc B = an_1
	 * Số lần chuyển 1 đĩa cuối cùng từ cọc A sang cọc C = 1
	 * Số lần chuyển n-1 đĩa từ cọc B sang cọc C = an_1
	 * ==> Số lần chuyển n đĩa từ cọc A sang cọc C = 2 *an_1 +1
	 * Giải hệ thức đệ quy an = 2 * an_1 + 1;
	 * ==> 2 * 2^n - 1
	 */
};

