/*
 * HopDong.cpp
 *
 *  Created on: Apr 30, 2019
 *      Author: ruby
 */

class HopDong {
public:
	int MaSo;
	int Nam;
	float GiaTri;

};

class DieuKienChon {
public:
	virtual bool DuocChon(HopDong hopdong)=0;
};

class ChonTheoMaSo: public DieuKienChon {
protected:
	int MaSo;

public:
	ChonTheoMaSo(int maso) {
		this->MaSo = maso;
	}

	bool DuocChon(HopDong hopdong) {
		return hopdong.MaSo == this->MaSo;
	}
};

class ChonTheoNam: public DieuKienChon {
protected:
	int Nam;

public:
	ChonTheoNam(int nam) {
		this->Nam = nam;
	}

	bool DuocChon(HopDong hopdong) {
		return hopdong.Nam == this->Nam;
	}
};

class ChonTheoGiaTri: public DieuKienChon {
protected:
	float MinGiaTri;
	float MaxGiaTri;

public:
	ChonTheoGiaTri(float min, float max) {
		this->MinGiaTri = min;
		this->MaxGiaTri = max;
	}

	bool DuocChon(HopDong hopdong) {
		return hopdong.GiaTri >= this->MinGiaTri
				&& hopdong.GiaTri <= this->MaxGiaTri;
	}
};

float TongGiaTri(HopDong dsHopDong[], int n, DieuKienChon * pDieuKien) {
	float result = 0;
	for (int i = 0; i < n; i++) {
		if (pDieuKien->DuocChon(dsHopDong[i])) {
			result += dsHopDong[i].GiaTri;
		}
	}
	return result;
}

