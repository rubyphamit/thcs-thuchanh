#include <stdio.h>
#include <stdbool.h>

class Exam2014Q1 {
	void testd() {
		/* fn = (17xn + 5yn )/n là hàm tăng */
		/* a=2014 --> fn >2014 */
		/*a = [1-2014]*/
		int n = 5;

		int left = 1, right = 2014;
		int a = (left + right) / 2;
		int result;
		while (left <= right) {
			if (getFn(n, a) >= 2014) {
				result = a;
				right = a - 1;
			} else {
				left = a + 1;
			}
			a = (left + right) / 2;
		}

		printf("result = %d \n", result);
	}

	int getFn(int n, int a) {

		int x = a;
		int y = 2 * a + 1;
		int preX = x;
		int preY = y;
		for (int i = 2; i <= n; i++) {
			x = preX + 2 * preY;
			y = preY + 2 * preX;
			preX = x;
			preY = y;

		}
		int fn = (17 * x + 5 * y) / n;
		printf("n=%d, a=%d --> fn=%d\n", n, a, fn);
		return fn;
	}
	void testb() {
		int a = 2;
		int n = 4;
		printf("a = %d, n = %d\n", a, n);

		int xn = calculateXn(n, a);
		printf("xn = %d\n", xn);
		int yn = calculateYn(n, a);
		printf("yn = %d\n", yn);
	}

	int calculateXn(int n, int a) {
		if (n == 1) {
			return a;
		}
		return calculateXn(n - 1, a) + 2 * calculateYn(n - 1, a);
	}

	int calculateYn(int n, int a) {
		if (n == 1) {
			return 2 * a + 1;
		}
		return calculateYn(n - 1, a) + 2 * calculateXn(n - 1, a);

	}

	void testc() {
		printf("sum = %d\n", sumXnAndYn(4, 2));

	}

	int sumXnAndYn(int n, int a) {
		int x = a;
		int y = 2 * a + 1;
		int preX = x;
		int preY = y;
		for (int i = 2; i <= n; i++) {
			x = preX + 2 * preY;
			y = preY + 2 * preX;
			preX = x;
			preY = y;

		}
		return x + y;
	}

};

