/*
 * Exam201501Q1.cpp
 *
 *  Created on: May 1, 2019
 *      Author: ruby
 */
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <iostream>
using namespace std;

class Exam201501Q1 {
public:
	float calculateBalanceB(int n, int a, float b, int c) {
		if (n == 0) {
			return a;
		}
		float xn = (1 + b) * calculateBalanceB(n - 1, a, b, c);
		if (n % 3 == 0) {
			xn += c;
		}
		return xn;
	}

	float calculateBalanceC(int n, int a, float b, int c) {
		float result = a;
		float pre;
		for (int i = 1; i <= n; i++) {
			pre = result;
			result = (1 + b) * pre;
			if (i % 3 == 0) {
				result += c;
			}
		}
		return result;
	}

	int define_a(int k, float b, int c, int M) {
		//a=[1,M]
		int left = 1, right = M, a = (left + right) / 2, result;
		while (left <= right) {
			if (calculateBalanceC(k, a, b, c) >= M) {
				result = a;
				right = a - 1;

			} else {
				left = a + 1;
			}
			a = (left + right) / 2;
		}
		return result;
	}
};

//int main(int argc, char **argv) {
//	int n = 4, a = 100000000, c = 50000;
//	float b = 0.01;
//	Exam201501Q1 test;
//	printf("Calculate B = %.6f\n", test.calculateBalanceB(n, a, b, c));
//	printf("Calculate C = %.6f\n", test.calculateBalanceC(n, a, b, c));
//	int M = 200000000;
//	printf("Define a = %d", test.define_a(12, b, c, M));
//}
