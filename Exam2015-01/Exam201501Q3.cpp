/*
 * Exam201501Q3.cpp
 *
 *  Created on: May 1, 2019
 *      Author: ruby
 */

#include <string>
#include <iostream>
using namespace std;

class DateExporter {
public:
	virtual void Output(int day, int month, int year)=0;
	/* a) Hàm thuần ảo*/

};

class DateExporter1: public DateExporter {
public:
	virtual void Output(int day, int month, int year) {
		cout << day << "/" << month << "/" << year;
	}
};

class DateExporter2: public DateExporter {
public:
	virtual void Output(int day, int month, int year) {
		cout << month << "-" << day << "-" << year;
	}
};

class Date {
private:
	int day, month, year;
public:
	void Output(string strFormat) {
		DateExporter * pExporter = NULL;
		if ("d/m/y" == strFormat) {
			pExporter = new DateExporter1;
		} else if ("m-d-y" == strFormat) {
			pExporter = new DateExporter2;
		}

		if (pExporter != NULL) {
			pExporter->Output(day, month, year);
		}
	}
};

