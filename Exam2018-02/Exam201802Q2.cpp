/*
 * Exam201802Q1.cpp
 *
 *  Created on: May 1, 2019
 *      Author: ruby
 */
#include <string>
#include <stdio.h>
#include <stdbool.h>
#include <iostream>
#include <functional>
using namespace std;

class Employee {
public:
	string Ten;
	virtual float calcSalary(float hours)=0;

};

class CEO: public Employee {
public:

	CEO(string ten) {
		this->Ten = ten;
	}
	float calcSalary(float hours) {
		return hours * 3;
	}
};

/*singleton*/
class SingletonCEO: public Employee {
private:
	string Ten;
	SingletonCEO(string ten) {
		this->Ten = ten;
	}
public:
	static SingletonCEO *instance ;

	static SingletonCEO* &getInstance() {
		return instance;
	}
	float calcSalary(float hours) {
		return hours * 3;
	}

	string getName() {
		return this->Ten;
	}
};

class Manager: public Employee {
public:
	Manager(string ten) {
		this->Ten = ten;
	}
	float calcSalary(float hours) {
		return hours * 2;
	}
};

class Engineer: public Employee {
public:
	Engineer(string ten) {
		this->Ten = ten;
	}
	virtual float calcSalary(float hours) {
		return hours * 1;
	}
};


int main(int argc, char **argv) {
	Employee * employees[3];
	employees[0] = new Manager("Dan");
	employees[1] = new Engineer("Jane");
	employees[2] = new CEO("Lee");

	int hours = 160;
	for (int i = 0; i < 3; i++) {
		cout << employees[i]->Ten << "=" << employees[i]->calcSalary(hours)
				<< endl;
	}

}
