/*
 * ExambkQ4.cpp
 *
 *  Created on: May 1, 2019
 *      Author: ruby
 */
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <iostream>

#include <map>

using namespace std;

class ExambkQ4 {

public:
	bool checkSymmetry(int a[], int n) {

		for (int i = 0; i < n / 2; i++) {
			if (a[i] != a[n - i - 1]) {
				return false;
			}
		}

		return true;
	}

	bool checkPotentialSymmetry(int a[], int n) {
		map<int, int> data;
		for (int i = 0; i < n; i++) {
//			printf("a%d = %d\n", i, a[i]);
			int value = data[a[i]] + 1;
			data.erase(a[i]);
			data.insert(pair<int, int>(a[i], value));

		}
		bool hasUnPair = false;
		bool isEven = (n % 2 == 0);
		for (map<int, int>::iterator it = data.begin(); it != data.end();
				++it) {
			bool newUnPair = false;
			if (it->second % 2 == 1) {
				newUnPair = true;
			}
			if (newUnPair) {
				if (isEven) {
					return false;
				} else if (hasUnPair) {
					return false;
				} else {
					hasUnPair = true;
				}
			}
		}

		return true;
	}

	int countRemoveElementToymmetry(int a[], int n) {
		int count = 0;
		map<int, int> data;
		for (int i = 0; i < n; i++) {
			//			printf("a%d = %d\n", i, a[i]);
			int value = data[a[i]] + 1;
			data.erase(a[i]);
			data.insert(pair<int, int>(a[i], value));

		}
		bool hasUnPair = false;
		for (map<int, int>::iterator it = data.begin(); it != data.end();
				++it) {
			bool newUnPair = false;
			int value = it->first;
			int noOfVal = it->second;
			if (noOfVal % 2 == 1) {
				newUnPair = true;
			}
			if (newUnPair) {
				if (n % 2 == 0) {
					printf("remove %d\n", value);
					count++;
					n--;
				} else if (hasUnPair) {
					printf("remove %d\n", value);
					count++;
					n--;
					hasUnPair = false;

				} else {
					hasUnPair = true;
				}
			}
		}
		printf("n = %d\n", n);
		return count;
	}

};

//int main() {
//	int n = 7;
//	int a[n] = { 1, 2, 3, 2, 1, 7, 2 };
//	ExambkQ4 test;
//	int isSym = test.countRemoveElementToymmetry(a, n);
//	printf("Count Remove Element To Symmetry %d\n", isSym);
//}
