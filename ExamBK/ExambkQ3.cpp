/*
 * ExambkQ3.cpp
 *
 *  Created on: May 1, 2019
 *      Author: ruby
 */

#include <stdio.h>
#include <vector>
#include <stdbool.h>

using namespace std;

class Point {
public:
	double X, Y;

	Point() {
		this->X = 0;
		this->Y = 0;
	}
	Point(double x, double y) {
		this->X = x;
		this->Y = y;
	}
};

class Shape {
public:
	virtual Shape *Clone()=0;
	virtual bool IsSelected(Point p)=0;
};

class Triangle: public Shape {
protected:
	Point A, B, C;

public:
	Triangle() {
	}
	Triangle(Point a, Point b, Point c) {
		this->A = a;
		this->B = b;
		this->C = c;
	}

	Shape *Clone() {
		Triangle * t;
		t->A = this->A;
		t->B = this->B;
		t->C = this->C;
		return t;
	}

	bool IsSelected(Point p) {
		return false;
	}
};

class Circle: public Shape {
protected:
	Point A;
	float R;

public:
	Circle() {
	}
	Circle(Point a, float r) {
		this->A = a;
		this->R = r;
	}

	Shape *Clone() {
		Circle * t;
		t->A = this->A;
		t->R = this->R;
		return t;
	}

	bool IsSelected(Point p) {
		return true;
	}
};

class ComplexShape: public Shape {
protected:
	vector<Shape*> Children;

public:
	bool IsSelected(Point p) {
		int size = Children.size();
		for (int i = 0; i < size; i++) {
			Shape * s = Children[i];
			if (s->IsSelected(p)) {
				return true;
			}

		}
		return false;
	}

	Shape *Clone() {
		Shape * s;

		return s;
	}

	void setChildren(vector<Shape*> _children) {
		this->Children = _children;
	}
};

//int main(int argc, char **argv) {
//	ComplexShape comShape;
//	vector<Shape*> shapes;
//	Shape* t;
//	for (int i = 0; i < 9; i++) {
//		if (i % 2 == 0) {
//			t = new Triangle;
//		} else {
//			t = new Circle;
//		}
//		shapes.push_back(t);
//	}
//	comShape.setChildren(shapes);
//
//	printf("%d", comShape.IsSelected(Point(0, 0)));
//}
