/*
 * Exam201702.cpp
 *
 *  Created on: May 2, 2019
 *      Author: ruby
 */

#include <iostream>
using namespace std;

class Exam201702 {
	/********** 1 *****************
	 *
	 * 	1	5	2
	 * 	4	6	3
	 * 	7	8	0
	 *
	 */

	/********** 2 *****************
	 *
	 * 	1	2	3			1	2	3
	 * 	4	5	6	-->		4	5	0	-->		1 2 3 4 5 0 7 8 6
	 * 	7	8	0			7	8	6
	 *
	 */

	/********** 3 *****************
	 * 1 0 2 4 5 6 7 8 3	(0 1 2 4 5 6 7 8 3)		(1 2 0 4 5 6 7 8 3)		(1 5 2 4 0 6 7 8 3)
	 *
	 * 	1	0	2				0	1	2				1	2	0				1	5	2
	 * 	4	5	6				4	5	6				4	5	6				4	0	6
	 * 	7	8	3				7	8	3				7	8	3				7	8	3
	 *
	 */

	/********** 4 *****************
	 *
	 * 	1	2	3
	 * 	4	5	6
	 * 	7	8	0
	 *
	 *
	 * 	1	2	3
	 * 	4	5	0
	 * 	7	8	6
	 *
	 *
	 * 	1	2	0
	 * 	4	5	3
	 * 	7	8	6
	 *
	 *
	 *	1	0	2
	 * 	4	5	3
	 * 	7	8	6
	 *
	 *
	 *	1	5	2
	 * 	4	0	3
	 *	7	8	6
	 */

	/********** 5 *****************
	 *
	 * 	1	2	3
	 * 	4	5	6
	 * 	7	8	0
	 *
	 *
	 *	1	3	2
	 * 	4	5	6
	 * 	7	8	0
	 *
	 *	Xét dãy số khi bỏ số 0: 12345678
	 *	Khi dịch chuyển số 0 qua trái phải thì thứ tự dãy số không đổi
	 *	Khi dịch chuyển số 0 lên xuống thì thứ tự dãy số thay đổi 2k bước (luôn là số chẵn)
	 *	23-->32 thay đổi 1 bước --> không thực hiện được
	 */
public:
	/********** 6 *****************/
	void StateToMatrix(int A[], int M[3][3]) {
		for (int i = 0; i < 9; i++) {
			int row = i / 3;
			int col = i % 3;
			M[row][col] = A[i];
		}
	}

	/********** 7 *****************/
	void MatrixToState(int M[3][3], int A[]) {
		for (int i = 0; i < 9; i++) {
			int row = i / 3;
			int col = i % 3;
			A[i] = M[row][col];
		}
	}

	/********** 8 *****************/
	int CountPosibleMoves(int A[]) {
		int count = 0;
		int k = 0;
		for (int i = 0; i < 9; i++) {
			if (A[i] == 0) {
				k = i;
				break;
			}
		}
		int row = k / 3;
		int col = k % 3;

		if (row == 1) {
			count += 2; // move up and down
		} else {
			count++; // move up or down only
		}

		if (col == 1) {
			count += 2; // move left and right
		} else {
			count++; // move left or right only
		}
		return count;
	}

	/********** 9 *****************/
	bool MoveeLeft(int A[], int B[]) {
		int k = 0;
		for (int i = 0; i < 9; i++) {
			B[i] = A[i];
			if (A[i] == 0) {
				k = i;
			}
		}
		int col = k % 3;
		if (col == 0) {
			return false;
		}
		B[k] = B[k + 1];
		B[k + 1] = 0;
		return true;
	}

	void print(int M[][3]) {
		cout << "************************" << endl;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				cout << M[i][j] << "\t";
			}
			cout << endl;
		}
	}

	void print(int A[]) {
		cout << "************************" << endl;
		for (int i = 0; i < 9; i++) {
			cout << A[i] << "\t";
		}
		cout << endl;
	}

	// 10
};

//int main(int argc, char **argv) {
//	Exam201702 test;
//	int A[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
//	int M[3][3];
//	test.StateToMatrix(A, M);
//	test.print(A);
//	test.print(M);
//	cout << "Posible moves = " << test.CountPosibleMoves(A) << endl;
//
//	int B[] = { 1, 0, 2, 3, 4, 5, 6, 7, 8 };
//	test.StateToMatrix(B, M);
//	test.print(B);
//	test.print(M);
//	cout << "Posible moves = " << test.CountPosibleMoves(B) << endl;
//
//	int C[] = { 1, 2, 3, 4, 0, 5, 6, 7, 8 };
//	test.StateToMatrix(C, M);
//	test.print(C);
//	test.print(M);
//	cout << "Posible moves = " << test.CountPosibleMoves(C) << endl;
//}

