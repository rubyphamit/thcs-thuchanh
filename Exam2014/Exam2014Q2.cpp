#include <iostream>

using namespace std;
struct ThiSinh {
	string HoTen;
	string SobaoDanh;
	float DiemMonCoBan;
	float DiemMonCoSo;
	bool NgoaiNgu;
	ThiSinh* pNext;

public:
	ThiSinh();

	ThiSinh* ThemThiSinh(ThiSinh*& pHead, string HoTen, string SoBaoDanh,
			float DiemMonCoBan, float DiemMonCoSo, bool NgoaiNgu) {

		ThiSinh * ts = new ThiSinh();
		ts->HoTen = HoTen;
		ts->SobaoDanh = SoBaoDanh;
		ts->DiemMonCoBan = DiemMonCoBan;
		ts->DiemMonCoSo = DiemMonCoSo;
		ts->NgoaiNgu = NgoaiNgu;
		ts->pNext = pHead;

		return ts;
	}

	int SoLuongThiDau(ThiSinh* pHead, float DiemChuan) {
		int count = 0;
		ThiSinh* ts = pHead;
		while (ts != NULL) {
			if (ts->DiemMonCoBan >= 5 && ts->DiemMonCoSo >= 5 && ts->NgoaiNgu
					&& (ts->DiemMonCoBan + ts->DiemMonCoSo) >= DiemChuan) {
				count++;
			}
			ts = ts->pNext;
		}
		return count;
	}

	float XacDinhDiemChuan(ThiSinh * pHead, int ChiTieu) {
//0 --> 20, làm tròn 0.5 --> 41 khoảng giá trị
		int list[41];
		ThiSinh * ts = pHead;
		while (ts != NULL) {
			int index = ts->DiemMonCoBan + ts->DiemMonCoSo;
			if (ts->NgoaiNgu) {
				list[index]++;
			}
			ts = pHead->pNext;
		}
		int count = 0;
		int i = 40;
		while (i > 19 && count < ChiTieu) {
			count += list[i];
			i--;
		}

		return float(i / 2);

	}
};

