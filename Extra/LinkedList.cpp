/*
 * LinkedList.cpp
 *
 *  Created on: May 6, 2019
 *      Author: ruby
 */

#include <iostream>
using namespace std;

struct Node {
public:
	int data;
	Node * pNext;

	/**
	 * append an element with val into head of the linkedlist
	 */
	bool appendToHead(Node *&pHead, int val) {
		Node *node = new Node;
		if (node == NULL) {
			return false;
		}
		node->data = val;
		node->pNext = pHead;
		pHead = node;
		return true;
	}

	/**
	 * append an element with val into tail of the linkedlist
	 */
	bool appendToTail(Node *&pHead, int val) {
		Node *node = new Node;
		if (node == NULL) {
			return false;
		}
		node->data = val;
		node->pNext = NULL;
		if (pHead == NULL) {
			pHead = node;
		} else {
			Node *tmp = pHead;
			while (tmp->pNext != NULL) {
				tmp = tmp->pNext;
			}
			tmp->pNext = node;
		}
		return true;
	}

	/**
	 * Remove the first element with val in the liskedlist
	 */
	bool remove(Node* &pHead, int val) {
		if (pHead == NULL) {
			return false;
		}
		if (pHead->data == val) {
			Node *tmp = pHead;
			pHead = pHead->pNext;
			delete tmp;
		}
		Node *pPre = pHead;
		Node *p = pPre->pNext;
		while (p != NULL) {

			if (p->data == val) {
				pPre->pNext = p->pNext;
				delete p;
				return true;
			}
			p = pPre->pNext;
		}
		return false;
	}

	/**
	 * insert a node into a sorted (ASCENDING) linkedlist
	 */
	void insertToSortedASC(Node *&pHead, Node * pNode) {
		if (pNode == NULL) {
			return;
		}
		if (pHead == NULL) {
			pHead = pNode;
			pNode->pNext = NULL;
			return;
		}
		Node* pPre = pHead;
		while (pPre->pNext != NULL && pPre->pNext->data < pNode->data) {
			pPre = pPre->pNext;
		}
		pNode->pNext = pPre->pNext;
		pPre->pNext = pNode;
	}

	void insertionSort(Node *&pHead) {
		Node *newpHead = NULL;
		while (pHead != NULL) {
			insertToSortedASC(newpHead, pHead);
			pHead = pHead->pNext;
		}
		pHead = newpHead;
	}

	void quickSort(Node *&pHead) {
		if (pHead == NULL || pHead->pNext == NULL) {
			return;
		}
		Node *pivot = pHead;
		Node *p1 = NULL;
		Node *p2 = NULL;
		pHead = pHead->pNext;

		while (pHead != NULL) {
			Node *p = pHead;
			pHead = pHead->pNext;
			if (p->data <= pivot->data) {
				appendToHead(p1, p);
			} else {
				appendToHead(p2, p);
			}
		}
		quickSort(p1);
		quickSort(p2);

		if (p1 != NULL) {
			pHead = p1;
		}
		if (pHead == NULL) {
			pHead = pivot;
		} else {
			Node * tail = getTail(pHead);
			tail->pNext = pivot;
		}
		pivot->pNext = p2;

	}

	Node * getTail(Node *&pHead) {
		if (pHead == NULL) {
			return NULL;
		}
		Node * pTail = pHead;
		while (pTail->pNext != NULL) {
			pTail = pTail->pNext;
		}
		return pTail;
	}

	bool appendToHead(Node *&pHead, Node *node) {
		node->pNext = pHead;
		pHead = node;
		return true;
	}

};
